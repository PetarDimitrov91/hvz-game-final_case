package pvd.hvzgamecase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerRequestDto {
    private boolean isHuman;
    private boolean isZombie;
    private String lng;
    private String lat;
}
