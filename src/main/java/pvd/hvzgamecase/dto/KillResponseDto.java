package pvd.hvzgamecase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KillResponseDto {
    private String killerUsername;
    private String victimUsername;
    private Long timeOfDeath;
}
