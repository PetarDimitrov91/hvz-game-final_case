package pvd.hvzgamecase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pvd.hvzgamecase.common.enums.GameState;
import pvd.hvzgamecase.models.Kill;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {
    private String username;
    private String firstname;
    private String lastname;
    private String biteCode;
    private Long userId;
    private Long playerId;
    private boolean isAdmin;
    private boolean isHuman;
    private boolean isZombie;
    private List<Kill> kills;
    private Long gameId;
    private Enum<GameState> gameState;
    private String gameName;
}
