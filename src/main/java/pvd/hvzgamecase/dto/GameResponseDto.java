package pvd.hvzgamecase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pvd.hvzgamecase.common.enums.GameState;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GameResponseDto {
    private Long id;
    private String name;
    private String description;
    private GameState state;
    private List<PlayerResponseDto> players;
}
