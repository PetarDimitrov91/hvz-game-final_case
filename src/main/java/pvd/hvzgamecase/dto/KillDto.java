package pvd.hvzgamecase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KillDto {
    private String victimBiteCode;
    private Long killer_id;
}
