package pvd.hvzgamecase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlayerResponseDto {
    private Long id;
    private Long userId;
    private Long gameId;
    private Boolean isHuman;
    private Boolean isZombie;
    private String biteCode;
    private String username;
    private String lng;
    private String lat;
}
