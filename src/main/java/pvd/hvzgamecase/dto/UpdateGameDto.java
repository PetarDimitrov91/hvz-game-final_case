package pvd.hvzgamecase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pvd.hvzgamecase.common.enums.GameState;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateGameDto {
    private String name;
    private GameState gameState;
    private String description;
}
