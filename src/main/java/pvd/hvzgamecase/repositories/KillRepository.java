
package pvd.hvzgamecase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pvd.hvzgamecase.models.Kill;

import java.util.List;
import java.util.Optional;

@Repository
public interface KillRepository extends JpaRepository<Kill, Long> {
    List<Kill> findKillsByGameId(Long game_id);

    Optional<Kill> findKillByGameIdAndId(Long game_id, Long id);
}
