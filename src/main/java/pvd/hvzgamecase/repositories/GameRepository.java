
package pvd.hvzgamecase.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pvd.hvzgamecase.models.Game;
import pvd.hvzgamecase.models.User;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
    void deleteGameByGameId(Long game_id);
}

