package pvd.hvzgamecase.controllers.game;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pvd.hvzgamecase.dto.GameResponseDto;
import pvd.hvzgamecase.dto.NewGameRequestDto;
import pvd.hvzgamecase.dto.UpdateGameDto;
import pvd.hvzgamecase.models.Game;
import pvd.hvzgamecase.services.GameService;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@AllArgsConstructor
@RestController
@RequestMapping("/api/game")
public class GameController {
    private final GameService gameService;

    @Secured("ROLE_admin-role")
    @PostMapping
    public ResponseEntity<GameResponseDto> createGame(@RequestBody NewGameRequestDto newGameRequestDto) {
        GameResponseDto game = this.gameService.saveGame(newGameRequestDto);

        return new ResponseEntity<>(game, CREATED);
    }

    @GetMapping()
    public ResponseEntity<List<GameResponseDto>> getAllGames() {
        List<GameResponseDto> games = this.gameService.getAllGames();

        return new ResponseEntity<>(games, OK);
    }

    @GetMapping("/{game_id}")
    public ResponseEntity<GameResponseDto> getGameById(@PathVariable Long game_id) {
        GameResponseDto game = this.gameService.getGameById(game_id);

        return new ResponseEntity<>(game, OK);
    }

    @Secured("ROLE_admin-role")
    @PutMapping("/{game_id}")
    public ResponseEntity<GameResponseDto> updateGame(
            @PathVariable Long game_id,
            @RequestBody UpdateGameDto updateGameDto) {

        GameResponseDto game = this.gameService.updateGame(game_id, updateGameDto);

        return new ResponseEntity<>(game, OK);
    }

    @Secured("ROLE_admin-role")
    @DeleteMapping("/{game_id}")
    public ResponseEntity<HttpStatus> deleteGame(@PathVariable Long game_id) {
        boolean isDeleted = this.gameService.deleteGame(game_id);
        if (!isDeleted) {
            return new ResponseEntity<>(NOT_FOUND);
        }

        return new ResponseEntity<>(NO_CONTENT);
    }


}
