package pvd.hvzgamecase.controllers.player;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pvd.hvzgamecase.dto.PlayerRequestDto;
import pvd.hvzgamecase.dto.PlayerResponseDto;
import pvd.hvzgamecase.services.PlayerService;

import java.util.List;

@RestController
@AllArgsConstructor
public class PlayerController {
    private final PlayerService playerService;

    @PostMapping("/api/game/{game_id}/player")
    public ResponseEntity<PlayerResponseDto> createPlayer(
            @PathVariable Long game_id,
            @RequestBody PlayerRequestDto playerRequestDto) {
        System.out.println(playerRequestDto);
        PlayerResponseDto player = this.playerService.registerPlayerForGame(game_id, playerRequestDto);

        return new ResponseEntity<>(player, HttpStatus.CREATED);
    }

    @GetMapping("/api/game/{game_id}/player")
    public ResponseEntity<List<PlayerResponseDto>> getAllPlayers(@PathVariable Long game_id) {
        List<PlayerResponseDto> players = this.playerService.getAllPlayers(game_id);

        return new ResponseEntity<>(players, HttpStatus.OK);
    }

    @GetMapping("/api/game/{game_id}/player/{player_id}")
    public ResponseEntity<PlayerResponseDto> getPlayerGameById(
            @PathVariable Long game_id,
            @PathVariable Long player_id) {
        PlayerResponseDto player = this.playerService.getPlayerById(game_id, player_id);

        return new ResponseEntity<>(player, HttpStatus.OK);
    }

    @Secured("ROLE_admin-role")
    @PutMapping("/api/game/{game_id}/player/{player_id}")
    public ResponseEntity<PlayerResponseDto> updatePlayerByGameId(
            @PathVariable Long game_id,
            @PathVariable Long player_id,
            @RequestBody PlayerRequestDto playerRequestDto
    ) {
        PlayerResponseDto player = this.playerService.updatePlayerById(game_id, player_id, playerRequestDto);

        return new ResponseEntity<>(player, HttpStatus.OK);
    }
}
