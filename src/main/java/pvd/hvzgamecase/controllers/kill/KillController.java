package pvd.hvzgamecase.controllers.kill;

import lombok.AllArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pvd.hvzgamecase.dto.KillDto;
import pvd.hvzgamecase.dto.KillResponseDto;
import pvd.hvzgamecase.models.Kill;
import pvd.hvzgamecase.services.KillService;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@AllArgsConstructor
@RestController
public class KillController {
    private final KillService killService;

    @Secured("ROLE_admin-role")
    @PostMapping("/api/game/{game_id}/kill")
    public ResponseEntity<Kill> createKill(
            @PathVariable Long game_id,
            @RequestBody KillDto killDto
    ) {
        Kill kill = this.killService.createKill(game_id, killDto);

        return new ResponseEntity<>(kill, CREATED);
    }

    @GetMapping("/api/game/{game_id}/kill")
    public ResponseEntity<List<KillResponseDto>> getKillsByGameId(@PathVariable Long game_id) {
        List<KillResponseDto> kills = this.killService.getKillsByGameId(game_id);

        return new ResponseEntity<>(kills, OK);
    }

    @GetMapping("/api/game/{game_id}/kill/{kill_id}")
    public ResponseEntity<Kill> getKillByGameIdAndKillId(
            @PathVariable Long game_id,
            @PathVariable Long kill_id
    ) {
        Kill kill = this.killService.getKillByGameIdAndKillId(game_id, kill_id);

        return new ResponseEntity<>(kill, OK);
    }

    @Secured("ROLE_admin-role")
    @DeleteMapping("/api/game/kill/{kill_id}")
    public ResponseEntity<HttpStatus> deleteKill(
            @PathVariable Long kill_id
    ) {

        this.killService.deleteKillByGameId(kill_id);
        return new ResponseEntity<>(NO_CONTENT);
    }
}
