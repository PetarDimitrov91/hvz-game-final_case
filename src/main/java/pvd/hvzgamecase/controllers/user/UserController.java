package pvd.hvzgamecase.controllers.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pvd.hvzgamecase.dto.UserRequestDto;
import pvd.hvzgamecase.dto.UserResponseDto;
import pvd.hvzgamecase.models.User;
import pvd.hvzgamecase.services.UserService;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@AllArgsConstructor
@RestController
@RequestMapping("/api/user")
public class UserController {
    private UserService userService;

    @PostMapping
    public ResponseEntity<User> createUser() {
        User user = userService.createUser();

        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = this.userService.getAllUsers();

        return new ResponseEntity<>(users, HttpStatus.CREATED);
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @GetMapping("/{username}")
    public ResponseEntity<UserResponseDto> getUserByUsername(@PathVariable String username) {
        UserResponseDto user = this.userService.getUserByUsername(username);
        return new ResponseEntity<>(user, OK);
    }

    @Secured("ROLE_admin-role")
    @PutMapping("/{user_id}")
    public ResponseEntity<User> updateGame(
            @PathVariable Long user_id,
            @RequestBody UserRequestDto userReq) {

        User user = this.userService.updateUser(user_id, userReq);

        return new ResponseEntity<>(user, OK);
    }
}
