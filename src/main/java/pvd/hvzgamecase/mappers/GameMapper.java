package pvd.hvzgamecase.mappers;

import pvd.hvzgamecase.dto.GameResponseDto;
import pvd.hvzgamecase.dto.PlayerResponseDto;
import pvd.hvzgamecase.models.Game;
import pvd.hvzgamecase.models.Player;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface GameMapper {

    Function<Game, GameResponseDto> mapPlayerToPlayerResponseDto = game -> {
        GameResponseDto gameRes = new GameResponseDto();
        gameRes.setId(game.getGameId());
        gameRes.setName(game.getName());
        gameRes.setState(game.getGameState());
        gameRes.setDescription(game.getDescription());

        List<PlayerResponseDto> players = game.getPlayers()
                .stream()
                .map(Player.mapPlayerToPlayerResponseDto)
                .collect(Collectors.toList());

        gameRes.setPlayers(players);


        return gameRes;
    };
}
