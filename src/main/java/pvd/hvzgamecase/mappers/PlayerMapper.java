package pvd.hvzgamecase.mappers;

import pvd.hvzgamecase.dto.PlayerResponseDto;
import pvd.hvzgamecase.models.Player;

import java.util.function.Function;

public interface PlayerMapper {
   Function<Player,PlayerResponseDto > mapPlayerToPlayerResponseDto = (player) -> {
        PlayerResponseDto playerRes = new PlayerResponseDto();
        playerRes.setId(player.getId());
        playerRes.setUserId(player.getUser().getId());
        playerRes.setGameId(player.getGame().getGameId());
        playerRes.setIsHuman(player.getIsHuman());
        playerRes.setIsZombie(player.getIsZombie());
        playerRes.setBiteCode(player.getBiteCode());
        playerRes.setUsername(player.getUsername());
        playerRes.setLat(player.getLat());
        playerRes.setLng(player.getLng());

        return playerRes;
    };
}