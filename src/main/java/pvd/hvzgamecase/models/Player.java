package pvd.hvzgamecase.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.lang.Nullable;
import pvd.hvzgamecase.mappers.PlayerMapper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@ToString
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Player implements PlayerMapper {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    private Boolean isHuman;

    @NotNull
    private Boolean isZombie;

    @NotNull
    private String username;

    @Nullable
    private String biteCode;

    @NotNull
    private String lng;

    @NotNull
    private String lat;

    @OneToOne()
    @JsonBackReference
    @NotNull
    @ToString.Exclude
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private User user;

    @ManyToOne()
    @JsonBackReference
    @JoinColumn(name = "gameId", referencedColumnName = "gameId")
    @ToString.Exclude
    @NotNull
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private Game game;

    @OneToMany(cascade = CascadeType.ALL)
    @ToString.Exclude
    @NotNull
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private List<Kill> kills = new ArrayList<>();
}
