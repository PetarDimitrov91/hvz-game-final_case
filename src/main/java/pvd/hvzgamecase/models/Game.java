package pvd.hvzgamecase.models;

import lombok.*;
import pvd.hvzgamecase.common.enums.GameState;
import pvd.hvzgamecase.mappers.GameMapper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;
import static pvd.hvzgamecase.common.enums.GameState.REGISTRATION;

@Getter
@Setter
@ToString
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Game implements GameMapper {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long gameId;

    @NotNull
    private String name;

    @NotNull
    @Column(name = "gameDescription")
    private String description;

    @Enumerated(EnumType.STRING)
    private GameState gameState = REGISTRATION;

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
    @ToString.Exclude
    private List<Player> players;
}
