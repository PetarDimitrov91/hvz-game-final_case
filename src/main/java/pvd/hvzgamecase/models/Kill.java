package pvd.hvzgamecase.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.time.Instant;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@ToString
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Kill {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    private Instant timeOfDeath;

    @NotNull
    private String victimBiteCode;

    @NotNull
    private Long victimId;

    @NotNull
    private Long gameId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonBackReference
    @JoinColumn(name = "player_id", referencedColumnName = "id")
    @ToString.Exclude
    @NotNull
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    Player player;
}
