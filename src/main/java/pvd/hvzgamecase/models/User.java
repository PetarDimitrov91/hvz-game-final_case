package pvd.hvzgamecase.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import net.bytebuddy.implementation.bind.annotation.Default;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.List;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "hvz_user",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"username"})})

public class User {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotBlank
    @NotEmpty
    @NotNull
    private String keycloakId;

    @NotNull
    @NotBlank
    @NotEmpty
    private String username;

    @NotBlank(message = "first name is required")
    private String firstName;

    @NotBlank(message = "last name is required")
    private String lastName;

    @Column(columnDefinition = "boolean default false")
    private Boolean isAdmin;

    @OneToOne(fetch = LAZY)
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    Player player;
}
