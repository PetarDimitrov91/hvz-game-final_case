package pvd.hvzgamecase.models;

import lombok.*;
import pvd.hvzgamecase.common.enums.ChatStatus;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Message {
    private String senderName;
    private String receiverName;
    private String message;
    private String date;
    private ChatStatus status;
}
