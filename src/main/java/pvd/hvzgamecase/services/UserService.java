package pvd.hvzgamecase.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pvd.hvzgamecase.common.exceptions.HvZException;
import pvd.hvzgamecase.dto.UserRequestDto;
import pvd.hvzgamecase.dto.UserResponseDto;
import pvd.hvzgamecase.models.Player;
import pvd.hvzgamecase.models.User;
import pvd.hvzgamecase.repositories.UserRepository;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;
    private AuthService authService;

    public User createUser() {
        authService.checkAuthority();
        boolean isAdmin = authService.isUserAdmin();
        String userId = authService.getUserId();
        String username = authService.getUsername();
        String firstname = authService.getFirstname();
        String lastname = authService.getLastname();

        User user = new User();

        user.setKeycloakId(userId);
        user.setUsername(username);
        user.setIsAdmin(isAdmin);
        user.setFirstName(firstname);
        user.setLastName(lastname);

        userRepository.save(user);

        return user;
    }

    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    @Transactional
    public boolean deleteUser(Long user_id) {
        try {
            this.userRepository.deleteUserById(user_id);
            return true;
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }

        return false;
    }

    @Transactional
    public User updateUser(Long user_id, UserRequestDto userReq) {

        User user = this.userRepository.findById(user_id)
                .orElseThrow(() -> new HvZException("User does not exist"));

        if (userReq.getFirstname() != null) {
            user.setFirstName(userReq.getFirstname());
        }

        if (userReq.getLastname() != null) {
            user.setLastName(userReq.getLastname());
        }

        if (userReq.getUsername() != null) {
            user.setUsername(userReq.getUsername());
        }

        return this.userRepository.save(user);
    }

    public UserResponseDto getUserByUsername(String username) {
        User user = this.userRepository.findByUsername(username)
                .orElseThrow(() -> new HvZException("User not found"));

        UserResponseDto userDto = new UserResponseDto();
        Player player = user.getPlayer();

        userDto.setUsername(user.getUsername());
        userDto.setFirstname(user.getFirstName());
        userDto.setLastname(user.getLastName());
        userDto.setUserId(user.getId());
        userDto.setAdmin(user.getIsAdmin());

        if (player != null) {
            userDto.setBiteCode(player.getBiteCode());
            userDto.setPlayerId(player.getId());
            userDto.setHuman(player.getIsHuman());
            userDto.setZombie(player.getIsZombie());
            userDto.setKills(player.getKills());
            userDto.setGameId(player.getGame().getGameId());
            userDto.setGameName(player.getGame().getName());
            userDto.setGameState(player.getGame().getGameState());
        }

        return userDto;
    }
}
