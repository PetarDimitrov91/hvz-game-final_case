package pvd.hvzgamecase.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pvd.hvzgamecase.common.exceptions.HvZException;
import pvd.hvzgamecase.dto.GameResponseDto;
import pvd.hvzgamecase.dto.NewGameRequestDto;
import pvd.hvzgamecase.dto.UpdateGameDto;
import pvd.hvzgamecase.models.Game;
import pvd.hvzgamecase.models.User;
import pvd.hvzgamecase.repositories.GameRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GameService {
    private final GameRepository gameRepository;

    public GameResponseDto saveGame(NewGameRequestDto newGameRequestDto) {
        Game game = new Game();
        game.setName(newGameRequestDto.getName());
        game.setDescription(newGameRequestDto.getDescription());
        game.setPlayers(new ArrayList<>());
        gameRepository.save(game);

        return Game.mapPlayerToPlayerResponseDto.apply(game);
    }

    public List<GameResponseDto> getAllGames() {
        return gameRepository.findAll()
                .stream()
                .map(Game.mapPlayerToPlayerResponseDto)
                .collect(Collectors.toList());
    }

    public GameResponseDto getGameById(Long game_id) {
        Game game = this.gameRepository.findById(game_id)
                .orElseThrow(() -> new HvZException("game does not exist"));

        return Game.mapPlayerToPlayerResponseDto.apply(game);
    }

    public GameResponseDto updateGame(Long game_id, UpdateGameDto updateGameDto) {
        Game game = this.gameRepository.findById(game_id)
                .orElseThrow(() -> new HvZException("game does not exist"));

        if (updateGameDto.getName() != null) {
            game.setName(updateGameDto.getName());
        }

        if (updateGameDto.getGameState() != null) {
            game.setGameState(updateGameDto.getGameState());
        }

        if (updateGameDto.getDescription() != null) {
            game.setDescription(updateGameDto.getDescription());
        }

        Game savedGame = this.gameRepository.save(game);

        return Game.mapPlayerToPlayerResponseDto.apply(savedGame);
    }

    @Transactional
    public boolean deleteGame(Long game_id) {
        try {
            this.gameRepository.findById(game_id)
                    .orElseThrow(() -> new HvZException("game not found"))
                    .getPlayers()
                    .forEach(player -> {
                        User user = player.getUser();
                        user.setPlayer(null);
                    });

            this.gameRepository.deleteGameByGameId(game_id);
            return true;
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
        return false;
    }
}
