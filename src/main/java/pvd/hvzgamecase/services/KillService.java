package pvd.hvzgamecase.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pvd.hvzgamecase.common.exceptions.HvZException;
import pvd.hvzgamecase.dto.KillDto;
import pvd.hvzgamecase.dto.KillResponseDto;
import pvd.hvzgamecase.models.Kill;
import pvd.hvzgamecase.models.Player;
import pvd.hvzgamecase.models.User;
import pvd.hvzgamecase.repositories.GameRepository;
import pvd.hvzgamecase.repositories.KillRepository;
import pvd.hvzgamecase.repositories.PlayerRepository;
import pvd.hvzgamecase.repositories.UserRepository;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class KillService {
    private final PlayerRepository playerRepository;
    private final KillRepository killRepository;
    private final GameRepository gameRepository;
    private final EntityManager entityManager;
    private final UserRepository userRepository;

    public Kill createKill(Long game_id, KillDto killDto) {
        Player victim = playerRepository.findByBiteCode(killDto.getVictimBiteCode())
                .orElseThrow(() -> new HvZException("Player does not exist"));

        Player killer = playerRepository.findById(killDto.getKiller_id())
                .orElseThrow(() -> new HvZException("Player does not exist"));

        if (!killer.getIsZombie() || !victim.getIsHuman()) {
            throw new HvZException("Player is not a zombie or victim is not a human");
        }

        gameRepository.findById(game_id)
                .orElseThrow(() -> new HvZException("Game does not exist"));

        Kill kill = new Kill();
        kill.setTimeOfDeath(Instant.now());
        kill.setVictimBiteCode(killDto.getVictimBiteCode());
        kill.setVictimId(victim.getId());
        kill.setGameId(game_id);
        kill.setPlayer(killer);

        victim.setIsZombie(true);
        victim.setIsHuman(false);
        victim.setBiteCode("");

        killer.getKills().add(kill);

        playerRepository.save(victim);
        killRepository.save(kill);

        return kill;
    }

    public List<KillResponseDto> getKillsByGameId(Long game_id) {

        return this.killRepository.findKillsByGameId(game_id)
                .stream()
                .map(kill -> {
                    KillResponseDto killRes = new KillResponseDto();

                    killRes.setKillerUsername(kill.getPlayer().getUsername());
                    killRes.setVictimUsername(this.playerRepository
                            .findById(kill.getVictimId())
                            .orElseThrow(() -> new HvZException("Something wrong while fetching the kill"))
                            .getUsername());
                    killRes.setTimeOfDeath(kill.getTimeOfDeath().toEpochMilli());

                    return killRes;
                }).collect(Collectors.toList());
    }

    public Kill getKillByGameIdAndKillId(Long game_id, Long kill_id) {
        return this.killRepository.findKillByGameIdAndId(game_id, kill_id)
                .orElseThrow(() -> new HvZException("No such kill object found"));
    }

    @Transactional
    public void deleteKillByGameId(Long kill_id) {

        Kill kill = killRepository.findById(kill_id)
                .orElseThrow(() -> new HvZException("Kill not exist"));

        Map<Long, Long> userPlayerRelKeyValuePair = new HashMap<>();
        List<User> users = userRepository.findAll();


        users.forEach(user -> {
            if (user.getPlayer() != null) {
                Long userId = user.getId();
                Long playerId = user.getPlayer().getId();
                userPlayerRelKeyValuePair.put(userId, playerId);
                user.setPlayer(null);
                userRepository.save(user);
            }
        });


        Kill killToDel = entityManager.find(Kill.class, kill_id);
        entityManager.remove(killToDel);

        userPlayerRelKeyValuePair.forEach((userId, playerId) -> {
            userRepository.findById(userId)
                    .ifPresent((user) -> {
                        try {
                            user.setPlayer(playerRepository.findById(playerId)
                                    .orElseThrow(() -> new HvZException("player not exist")));

                            userRepository.save(user);
                        } catch (HvZException e) {
                            e.printStackTrace();
                        }

                    });
        });

    }
}
