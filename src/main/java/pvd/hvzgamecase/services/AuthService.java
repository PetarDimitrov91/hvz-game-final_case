package pvd.hvzgamecase.services;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Data
public class AuthService {
    private static final AuthService instance = new AuthService();

    private boolean isAdmin;
    private String userId;
    private String username;
    private String firstname;
    private String lastname;

    private AuthService() {
        this.isAdmin = false;
        this.userId = "";
    }

    public static AuthService getInstance() {
        return instance;
    }

    public void checkAuthority() {
        Jwt principal = (Jwt) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();


        Map<String, Object> claims = principal.getClaims();

        this.userId = "";
        this.isAdmin = false;


        this.isAdmin = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .anyMatch(e -> e.equals("ROLE_admin-role"));

        for (var entry : claims.entrySet()) {
            String entryStr = entry.toString();
            String[] entrySet = entryStr.split("=");

            if (entrySet[0].equals("sub")) {
                this.userId = entrySet[1];
            }

            if (entrySet[0].equals("preferred_username")) {
                this.username = entrySet[1];
            }

            if (entrySet[0].equals("given_name")) {
                this.firstname = entrySet[1];
            }

            if (entrySet[0].equals("family_name")) {
                this.lastname = entrySet[1];
            }
        }
    }

    public boolean isUserAdmin() {
        return this.isAdmin;
    }

    public String getUserId() {
        return this.userId;
    }
}
