package pvd.hvzgamecase.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pvd.hvzgamecase.common.enums.GameState;
import pvd.hvzgamecase.common.exceptions.HvZException;
import pvd.hvzgamecase.dto.PlayerRequestDto;
import pvd.hvzgamecase.dto.PlayerResponseDto;
import pvd.hvzgamecase.models.Game;
import pvd.hvzgamecase.models.Player;
import pvd.hvzgamecase.models.User;
import pvd.hvzgamecase.repositories.GameRepository;
import pvd.hvzgamecase.repositories.PlayerRepository;
import pvd.hvzgamecase.repositories.UserRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PlayerService {
    private final UserRepository userRepository;
    private final PlayerRepository playerRepository;
    private final AuthService authService;
    private final GameRepository gameRepository;

    public PlayerResponseDto registerPlayerForGame(Long game_id, PlayerRequestDto playerRequestDto) {
        this.authService.checkAuthority();
        String userKeycloakId = this.authService.getUserId();

        Game game = gameRepository.findById(game_id)
                .orElseThrow(() -> new HvZException("Game does not exist"));

        GameState gameState = game.getGameState();

        if (!gameState.equals(GameState.REGISTRATION)) {
            throw new HvZException("Player cannot be registered in game with status " + gameState);
        }

        Player player = new Player();

        User user = userRepository.findByKeycloakId(userKeycloakId)
                .orElseThrow(() -> new HvZException("User does not exist"));


        boolean isUserInGame = this.playerRepository.findAll()
                .stream()
                .anyMatch(p -> p.getUsername().equals(user.getUsername()));

        if (isUserInGame) {
            throw new HvZException("User is already registered for a game");
        }

        if (playerRequestDto.isHuman()) {
            String biteCode = this.generateBiteCode();
            Optional<Player> optPlayer = playerRepository.findByBiteCode(biteCode);

            while (optPlayer.isPresent()) {
                biteCode = this.generateBiteCode();
            }

            player.setBiteCode(biteCode);
        } else {
            player.setBiteCode("");
        }

        user.setPlayer(player);
        player.setIsHuman(playerRequestDto.isHuman());
        player.setIsZombie(playerRequestDto.isZombie());
        player.setGame(game);
        player.setUser(user);
        player.setUsername(authService.getUsername());
        player.setLat(playerRequestDto.getLat());
        player.setLng(playerRequestDto.getLng());
        playerRepository.save(player);

        return Player.mapPlayerToPlayerResponseDto.apply(player);
    }

    public String generateBiteCode() {
        Random r = new Random();
        return String.format("%d", ((1 + r.nextInt(9)) * 10000 + r.nextInt(10000)));
    }

    public List<PlayerResponseDto> getAllPlayers(Long game_id) {

        return gameRepository.findById(game_id)
                .orElseThrow(() -> new HvZException("Game does not exists"))
                .getPlayers()
                .stream()
                .map(Player.mapPlayerToPlayerResponseDto)
                .collect(Collectors.toList());
    }

    public PlayerResponseDto getPlayerById(Long game_id, Long player_id) {
        return this.gameRepository.findById(game_id)
                .orElseThrow(() -> new HvZException("Game not exists"))
                .getPlayers()
                .stream()
                .map(Player.mapPlayerToPlayerResponseDto)
                .filter(e -> Objects.equals(e.getId(), player_id))
                .findFirst()
                .orElseThrow(() -> new HvZException("Player not exists"));
    }

    public PlayerResponseDto updatePlayerById(Long game_id, Long player_id, PlayerRequestDto playerRequestDto) {
        Player player = this.gameRepository.findById(game_id)
                .orElseThrow(() -> new HvZException("Game not exists"))
                .getPlayers()
                .stream()
                .filter(e -> Objects.equals(e.getId(), player_id))
                .findFirst()
                .orElseThrow(() -> new HvZException("Player not exists"));

        if (playerRequestDto.isZombie()) {
            player.setBiteCode("");
        }

        player.setIsZombie(playerRequestDto.isZombie());
        player.setIsHuman(playerRequestDto.isHuman());

        Player updatedPlayer = playerRepository.save(player);

        return Player.mapPlayerToPlayerResponseDto.apply(updatedPlayer);
    }
}
