package pvd.hvzgamecase.common.exceptions;

public class HvZException extends RuntimeException {
    public HvZException(String message){
        super(message);
    }
}
