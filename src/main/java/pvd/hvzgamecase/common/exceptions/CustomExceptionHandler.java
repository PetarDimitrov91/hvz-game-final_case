package pvd.hvzgamecase.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class CustomExceptionHandler {
        @ExceptionHandler(HvZException.class)
        public ResponseEntity<?> dataNotFoundExceptionHandling(Exception exception, WebRequest request) {
            return new ResponseEntity<>(new ErrorDetails(new Date(), exception.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
        }

        @ExceptionHandler(Exception.class)
        public ResponseEntity<?> globalExceptionHandling(Exception exception, WebRequest request) {
            HttpStatus httpStatus;
            if(exception instanceof AccessDeniedException){
                httpStatus = HttpStatus.FORBIDDEN;
            }else{
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            }
            return new ResponseEntity<>(new ErrorDetails(new Date(), exception.getMessage(), request.getDescription(false)), httpStatus);
        }
    }

