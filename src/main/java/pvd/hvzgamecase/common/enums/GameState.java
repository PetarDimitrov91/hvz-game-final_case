package pvd.hvzgamecase.common.enums;

public enum GameState {
    REGISTRATION,
    IN_PROGRESS,
    COMPLETE
}
