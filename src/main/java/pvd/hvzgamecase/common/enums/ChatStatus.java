package pvd.hvzgamecase.common.enums;

public enum ChatStatus {
    JOIN,
    MESSAGE,
    LEAVE
}
