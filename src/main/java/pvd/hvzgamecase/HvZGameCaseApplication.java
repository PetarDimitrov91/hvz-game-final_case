package pvd.hvzgamecase;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Async
@SpringBootApplication
public class HvZGameCaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(HvZGameCaseApplication.class, args);
    }


}
